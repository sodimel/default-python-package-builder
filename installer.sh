#!/bin/bash

# you can edit all of these params:
venv_name=".venv"

package_name="mypackage"
package_entry_point=""

package_version="0.0.1"
package_description="I use those files when creating a new python-based project from scratch. It helps me gain some minutes/hours of config to get is working. Maybe you want to use it too ?"
package_url="https://git.bitmycode.com/sodimel/default-python-package-creator"
package_author="Corentin Bettiol"
package_email="corentin@244466666.xyz"
package_license="WTFPL2"

setup_cfg_content = """
[metadata]
name = $package_name
version = $package_version
description = $package_description
long_description = file: README.md
long_description_content_type = text/markdown
url = $package_url
author = $package_author
author_email = $package_email
classifiers =
    License :: $package_license
    Programming Language :: Python
    Programming Language :: Python :: 3
[options]
include_package_data = true
packages = find:
"""

clear

printf "default-python-package-builder\n"
echo "------------------------------"

printf " [\e[32m✔️     \e[0m]  Creating setup.cfg file..."

rm setup.cfg
echo $setup_cfg_content > setup.cfg

printf "\r\033[0K [\e[32m✔️✔️    \e[0m]  Creatinv venv..."

python3 -m venv $venv_name
source $venv_name/bin/activate

# \r is carriage return
# \033[0K is to erase text up to the end of line 
printf "\r\033[0K [\e[32m✔️✔️✔️   \e[0m]  Installing wheel..."

python3 -m pip install wheel > /dev/null

printf "\r\033[0K [\e[32m✔️✔️✔️✔️  \e[0m]  Compiling package from sources..."

python3 setup.py bdist_wheel > /dev/null
rm -r build *.egg-info
mv dist/* .
rm -r dist

printf "\r\033[0K [\e[32m✔️✔️✔️✔️✔️ \e[0m]  Installing package..."

python3 -m pip install *.whl > /dev/null
rm *.whl

printf "\r\033[0K [\e[32m✔️✔️✔️✔️✔️✔️\e[0m]  Installation complete !"

echo ""

if [[ ! -z  $package_entry_point  ]]; then
	echo " Launching package entry point..."
	sleep 1
	clear
	python3 -m $package_name.$package_entry_point
	deactivate
else
	echo "Entry point is empty, my job here is done!"
	echo ""
	echo " Launch venv:"
	echo ". $venv_name/bin/activate"
	echo ""
	echo " Package name:"
	echo "$package_name"
fi
