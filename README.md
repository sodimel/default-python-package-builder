# default python package builder

Here's my code to create a python package, and install it in a venv, using `python3` & `python3-venv`.

----

## Requirements

 * Any linux distro
 * `bash`
 * `python3`
 * `python3-pip`
 * `python3-venv`


----

## Usage

```bash
./installer.sh
```

----

## Configuration

List of things you can customize:

| variable name | description |
|---|---|
| `package_name` | The name of your package (folder). Default = mypackage. |
| `package_entry_point` | Name of the file that will be executed. Default = mypackage. Empty the var to not launch the package at the end of the install process. |
| `package_version` | Package version. |
| `package_description` | Short description of your package. |
| `package_url` | Custom source url (can be a website, repo url, ...). |
| `package_author` | Author name. |
| `package_email` | Author email. |
| `package_license` | Licence of the package. See [classifiers doc](https://pypi.org/classifiers/) on pypi for more informations. |

----

## Demo video ?

Sure.

![](https://up.l3m.in//files/1584704547-dppb-0.0.1.webm)

----

*[More informations on cfg files](https://docs.python.org/3/distutils/configfile.html)*.
